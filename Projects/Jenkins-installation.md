# Installing jenkins in ubuntu server
## server
create a server in digital ocean an open port 8080
ssh root@<ip-adress>
apt update
## Install docker in the server
snap install docker
docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts
docker ps
## to get the password to the Jenkins admin user
docker exec -it <container id>
cat /var/jenkins_home/secrets/initialAdminPassword
# web browser
ip-adress:8080
install suggested pluggins