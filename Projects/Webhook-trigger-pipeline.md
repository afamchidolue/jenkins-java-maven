# Configure Jenkins CI to trigger CI Pipeline-Jobs automatically on every change
## jenkins configuration
install GitLab pluggins
go to manage jenkins-configuresystem-GitLab
fill the connection name tab "gitlab-conn"
paste https//gitlab.com/ in the Gitlab host URL tab
Add a credential by Creating  a GitLab API token 
To create a Gitlab api token
go to your profile in gitlab , select preference 
Select Access token and fill in the name, expiring date and configure the permission the access token will give jenkins API
Create personnel access Token and copy and save the token
ADD the Credentials
## configure GitLab to automatically send notification whenever a push or commits happened
Navigate to the repository's Settings > Integrations page in GitLab.
Scroll down to the  click "JENKINS CI".
Enable the integration by making it active
CLick on push
Paste your JENKINS URL http://142.93.149.162:8080/
project name is your pipeline name "my-pipeline"
username and password are actually the username and password of Jenkins
save changes



# Configure Webhook to trigger CI multibranchPipeline automatically on every change
## jenkins configuration
install Multibranch scan pluggin
go to the mulibranch pipeline- job and click configure
Check the scan by webhook
specify the name of the "trigger token" it could be anything e.g gitlab "token"
# configure GitLab to automatically send notification whenever a push or commits happened
Navigate to the repository's Settings > Integrations page in GitLab.
select Webhooks
configure http://142.93.149.162:8080/multibranch-webhook-trigger/invoke?token=gitlabtoken
add webhook